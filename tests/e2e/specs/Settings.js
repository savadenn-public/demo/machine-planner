// https://docs.cypress.io/api/introduction/api.html

describe('Settings', function () {
  it('access settings page', () => {
    cy.viewport(1920, 1080)
    cy.visit('/')

    // Click on Settings button
    cy.get('.Footer__SettingsLink').click()

    cy.get('main').should('have.class', 'SettingsView')
  })

  it('changes language to french', () => {
    cy.get('.SettingsView__Languages legend').should('contain', 'Language')
    cy.get('.SettingsView__Languages .Radio__OptionInput').check('fr')
    cy.get('.SettingsView__Languages legend').should('contain', 'Langue')
  })

  it('restores lang', () => {
    cy.get('.SettingsView__Languages .Radio__Restore').click()
    cy.get('.SettingsView__Languages legend').should('contain', 'Language')
  })

  it('changes theme to dark', () => {
    cy.get('.SettingsView').should(
      'have.css',
      'background-color',
      'rgb(255, 255, 255)'
    )
    cy.get('.SettingsView__Themes .Radio__OptionInput').check('themeDark')
    cy.get('.SettingsView').should(
      'have.css',
      'background-color',
      'rgb(81, 81, 81)'
    )
  })

  it('restores theme', () => {
    cy.get('.SettingsView__Themes .Radio__Restore').click()
    cy.get('.SettingsView').should(
      'have.css',
      'background-color',
      'rgb(255, 255, 255)'
    )
  })
})
