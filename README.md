# Time Manager App

## Development

```console
make dev
```

Then connect to https://time-manager.docker.localhost/

In order to use `make dev`, you need [this ansible role](https://gitlab.com/savadenn-public/ansible-roles/local-dev/-/tree/stable/docker)

## Other commands

```console
▶ make
build                          Build production app
clean                          Cleans up environnement
dev                            Starts dev stack
doc                            Generates docs in all format in ./public
generate_png                   Generates the image from svg files
help                           Prints this help
optimize_svg                   Optimize inkscape logo
pull                           Retrieves latest image
sh                             Runs command inside container
test.coverage                  Runs unit tests with code coverage
test.e2e.interactive           Runs end-to-end tests with interactive GUI
test.e2e                       Runs end-to-end tests
test.unit                      Runs unit tests
vue                            Open bash in container loaded with vue-cli
```
