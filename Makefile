#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
PROJECT_NAME := $(shell basename ${PROJECT_PATH})
export PROJECT_NAME
PROJECT_URL=${PROJECT_NAME}.docker.localhost
export PROJECT_URL
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

# command name that are also directories
.PHONY: dev

#-----------------------------------------
# Allow passing arguments to make
#-----------------------------------------
SUPPORTED_COMMANDS := dev run
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))
ifneq "$(SUPPORTS_MAKE_ARGS)" ""
  COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(COMMAND_ARGS):;@:)
endif

#-----------------------------------------
# Help commands
#-----------------------------------------
.PHONY: help
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_\-\0.0-9]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

#-----------------------------------------
# Commands
#-----------------------------------------
clean: ## Cleans up environnement
	@mkdir -p ./dist/docs && rm -rf ./dist/docs/*
	@docker-compose down --remove-orphans
	@docker-compose -f tests/e2e/docker-compose.yml -f tests/e2e/docker-compose.interactive.yml down --remove-orphans

dev: clean ## Starts dev stack
	@docker-compose build --pull
	@docker-compose up

doc: ## Generates docs in all format in ./public
	@mkdir -p ./dist/docs && rm -rf ./dist/docs/*
	@docker-compose run --rm -e "SPI_WATCH=false" -e "SPI_FORMAT=all" doc

build: ## Build production app
	@docker-compose run --rm app bash -c 'yarn build'

pull: ## Retrieves latest image
	@docker-compose pull

vue: ## Open bash in container loaded with vue-cli
	@docker-compose run --rm vue bash

test.unit: ## Runs unit tests
	@docker-compose run --rm app bash -c 'yarn run test:unit'

test.coverage: ## Runs unit tests with code coverage
	@docker-compose run --rm app bash -c 'yarn run test:coverage'

test.e2e: ## Runs end-to-end tests
	@cd ./tests/e2e && \
	docker-compose up cypress
	@cd ./tests/e2e && docker-compose down --remove-orphans

test.e2e.interactive: ## Runs end-to-end tests with interactive GUI
	@cd ./tests/e2e && \
	docker-compose -f docker-compose.yml -f docker-compose.interactive.yml up cypress

sh: ## Runs command inside container
	@docker-compose run --rm app bash


#-----------------------------------------
# Images management
#-----------------------------------------
scour := registry.gitlab.com/savadenn-public/scour:latest
optimize_svg: ## Optimize inkscape logo
	@docker pull ${scour} || true
	@rm -f ./public/img/*.g.svg
	@docker run --rm \
    --volume "$(shell pwd):/app" -w "/app" \
		-u "$(shell id -u ${USER}):$(shell id -g ${USER})" \
		${scour} \
    -i /app/public/img/logo.inkscape.svg -o /app/public/img/logo.g.svg --enable-viewboxing --enable-id-stripping --enable-comment-stripping --shorten-ids --indent=none --remove-descriptive-elements
	@sed 's/<g/<g fill="#fff"/g' ./public/img/logo.g.svg > ./public/img/logo.i.g.svg

inkscape := registry.gitlab.com/savadenn-public/inkscape:latest
generate_png: optimize_svg ## Generates the image from svg files
	@rm -f ./public/img/*.g.png
	@docker pull ${inkscape} || true
	@docker run --rm \
		--volume "$(shell pwd):/app" -w "/app" \
		-u "$(shell id -u ${USER}):$(shell id -g ${USER})" \
		${inkscape} \
		sh -c '/usr/bin/inkscape --shell < /app/public/img/generate.txt'
