// eslint-disable-next-line
const StyleLintPlugin = require('stylelint-webpack-plugin');


// noinspection JSUnusedGlobalSymbols
module.exports = {
  publicPath: './',
  configureWebpack: {
    devtool: 'source-map',
    devServer: {
      compress: true,
      public: process.env.PROJECT_URL,
    },
    plugins: [
      new StyleLintPlugin({
        files: 'src/**/*.{vue,htm,html,css,sss,less,scss,sass}',
        fix: true,
      }),
    ],
  },

  chainWebpack: (config) => {
    // Inline svg
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule.use('raw-loader').loader('raw-loader')
  },

  pwa: {
    name: 'Time Manager',
    themeColor: '#006a96',
    msTileColor: '#006a96',
    iconPaths: {
      favicon32: 'img/favicon-32x32.g.png',
      favicon16: 'img/favicon-16x16.g.png',
      appleTouchIcon: 'img/logo_i_256px.g.png',
      maskIcon: 'img/logo.g.svg',
      msTileImage: 'img/logo_i_256px.g.png',
    },
  },
  productionSourceMap: false,
}
