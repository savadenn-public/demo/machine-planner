import { shallowMount } from '@vue/test-utils'
import Button from '@/components/Button.vue'

describe('Components/Button.vue', () => {
  it('renders label when passed', () => {
    const label = 'my button'
    const wrapper = shallowMount(Button, { props: { label } })
    expect(wrapper.text()).toMatch(label)
  })

  it('triggers action when clicked', () => {
    const action = jest.fn()
    const wrapper = shallowMount(Button, {
      props: { label: '' },
      attrs: { onclick: action },
    })
    wrapper.trigger('click')
    expect(action.mock.calls.length).toEqual(1)
  })
})
