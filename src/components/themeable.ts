import { inject, ComputedRef } from 'vue'
import { Theme } from '@/types'

export function theme() {
  return {
    theme: inject<ComputedRef<Theme>>('theme'),
  }
}
