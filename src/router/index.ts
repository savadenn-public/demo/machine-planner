/* istanbul ignore file */
import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import { Home, Settings, Machine } from '@/views'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
  },
  {
    path: '/machine/:id',
    name: 'Machine',
    component: Machine,
    props: true,
  },
]

export const Router = createRouter({
  history: createWebHashHistory(),
  routes,
})
