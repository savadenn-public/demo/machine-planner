import { Entries } from '@/stores'
import * as Types from '@/types'
import { reactive, watch } from 'vue'
import * as Helpers from '@/helpers'

/**
 * Current entry
 */
export const Entry = reactive<Types.States.Entry>({
  latestAtStart: false,
  current: false,
  /**
   * Stop current entry
   */
  stop: async function () {
    console.log('State.Entry', 'stop')
    if (Entry.current) Helpers.Entry.stop(Entry.current)
  },

  /**
   * Start a new entry as current one
   */
  start: async function () {
    console.log('State.Entry', 'start')
    const newEntry = Helpers.Entry.new()
    if (Entry.current) {
      newEntry.before = Entry.current.id
      Entry.current.after = newEntry.id
      await Entry.stop()
    }
    Entry.current = newEntry
    console.log('State.Entry', { entry: Entry.current })
  },
})

/**
 * Auto save of changes on current entry
 */
watch(Entry, (entry) => {
  console.log('Change on current', { entry })
  if (entry.current) {
    Entries.put(entry.current).catch((e) =>
      console.error('loadLatest error', { e })
    )
    if (entry.current.stop) {
      entry.current = false
    }
  } else if (entry.latestAtStart && !entry.latestAtStart.stop) {
    console.log('Found latest entry', { entry })
    Entry.current = entry.latestAtStart
  }
})

/**
 * Load latest entry from database
 */
export async function loadLatest() {
  const store = await Entries.storeRead()
  const cursor = await store.index('by-start').openCursor(undefined, 'prev')

  if (cursor && cursor.value) Entry.latestAtStart = cursor.value
}
