import { computed, reactive, watchEffect } from 'vue'
import { States, Theme } from '@/types'

export const DEFAULT_SETTINGS: States.Settings = {
  lang: States.SAME_AS_BROWSER,
  theme: States.SAME_AS_BROWSER,
  downloadFormat: 'yaml',
}

export const settings: States.Settings = reactive({ ...DEFAULT_SETTINGS })

// Compute locale from browser settings
export const locale = computed(() =>
  settings.lang === States.SAME_AS_BROWSER
    ? window.navigator.language.substr(0, 2)
    : settings.lang
)

// Compute theme from browser settings
export const theme = computed<Theme>(() => {
  if (settings.theme === States.SAME_AS_BROWSER) {
    return matchMedia('(prefers-color-scheme: dark)').matches
      ? 'themeDark'
      : 'themeLight'
  }
  return settings.theme as Theme
})

// Load saved settings
const SETTINGS_KEY = 'settings'
const savedSettings = localStorage.getItem(SETTINGS_KEY)
if (savedSettings) {
  Object.assign(settings, JSON.parse(savedSettings))
}

// On change save settings
watchEffect(() => localStorage.setItem(SETTINGS_KEY, JSON.stringify(settings)))
