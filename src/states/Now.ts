import moment from 'moment'
import { ref } from 'vue'

export const Now = ref(moment())

function updateNow() {
  Now.value = moment()
  window.setTimeout(updateNow, 250)
}
updateNow()
