import { computed, reactive } from 'vue'
import * as Types from '@/types'
import { Machine as HelperMachine, Order as HelperOrder } from '@/helpers'
import moment from 'moment'

export const workshop: Types.Workshop = reactive({
  machines: {},
  orders: {},
})

export const backlogOrders = computed(() =>
  Object.values(workshop.orders).filter((order) => !order.schedule)
)

function addMachine(machine: Types.Machine) {
  machine.orders = computed(() =>
    Object.values(workshop.orders).filter(
      (order) => order.schedule && order.schedule.machineId === machine.id
    )
  )
  workshop.machines[machine.id] = machine
  return machine
}

function addOrder() {
  const order = HelperOrder.new()
  workshop.orders[order.id] = order
  return order
}

const m1 = addMachine(HelperMachine.new('Machine FIDIA GTF/Q'))
const m2 = addMachine(HelperMachine.new('Machine DMG DMU200P HSM'))
const m3 = addMachine(HelperMachine.new('Machine DMG DMU125P'))

let date = moment().subtract(2, 'h')

HelperOrder.schedule(addOrder(), date.toDate(), m1.id)
HelperOrder.schedule(addOrder(), date.toDate(), m2.id)
date = date.add(1, 'h')
HelperOrder.schedule(addOrder(), date.toDate(), m1.id)
HelperOrder.schedule(addOrder(), date.toDate(), m3.id)
date = date.add(2, 'h')
HelperOrder.schedule(addOrder(), date.toDate(), m2.id)
HelperOrder.schedule(addOrder(), date.toDate(), m3.id)
date = date.add(2, 'h')
HelperOrder.schedule(addOrder(), date.toDate(), m1.id)
HelperOrder.schedule(addOrder(), date.toDate(), m3.id)
date = date.add(2, 'h')
HelperOrder.schedule(addOrder(), date.toDate(), m1.id)
HelperOrder.schedule(addOrder(), date.toDate(), m3.id)
date = date.add(1, 'h')
HelperOrder.schedule(addOrder(), date.toDate(), m1.id)
HelperOrder.schedule(addOrder(), date.toDate(), m2.id)

addOrder()
addOrder()
addOrder()
addOrder()
addOrder()
addOrder()
addOrder()
addOrder()
