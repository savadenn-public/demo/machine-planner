export * from './PwaUpdate'
export * from './Settings'
export { Entry } from './Entry'
export * from './Now'
export * from './Workshop'

import { loadLatest } from './Entry'

/**
 * Initialize states asynchronously
 */
export async function initialize() {
  return Promise.all([loadLatest()])
}
