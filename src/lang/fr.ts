import { Translations } from '@/lang'

const message: Translations = {
  _name: 'Français',
  download: 'Télécharger les entrées',
  pwa: {
    available: 'Une mise à jour est disponible',
    update: 'Mettre à jour',
  },
  home: {
    view: 'Page principale',
  },
  navbar: {
    toggle: 'Afficher le menu',
  },
  settings: {
    title: 'Paramètres',
    language: 'Langue',
    reset: 'Utiliser la configuration par défaut',
    theme: 'Thème',
    sameAsBrowser: 'Utiliser la configuration navigateur',
    restore: 'Annuler toutes les modifications',
    light: 'Clair',
    dark: 'Sombre',
  },
  component: {
    restore: 'Annuler la modification',
  },
  entry: {
    content: 'Description',
    new: 'Nouvelle entrée',
    remove: 'Supprimer cette entrée',
    stop: 'Arrêter',
    off: 'Absent',
    edit: 'Modifier cet entrée',
    save: 'Enregistrer',
    date: {
      start: 'Date & heure de début',
      stop: 'Date & heure de fin',
    },
  },
}

export const fr = (message as unknown) as { [name: string]: string }
