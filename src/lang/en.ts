export const message = {
  _name: 'English',
  download: 'Download entries',
  pwa: {
    available: 'Update is available',
    update: 'Update now',
  },
  home: {
    view: 'Home',
  },
  navbar: {
    toggle: 'Toggle menu',
  },
  settings: {
    title: 'Settings',
    language: 'Language',
    reset: 'Use default settings',
    theme: 'Theme',
    sameAsBrowser: 'Use browser settings',
    light: 'Light',
    dark: 'Dark',
    restore: 'Restore previous settings',
  },
  component: {
    restore: 'Cancel change',
  },
  entry: {
    content: 'Description',
    new: 'New time entry',
    remove: 'Remove',
    stop: 'Stop',
    off: 'Away',
    edit: 'Edit entry',
    save: 'Save',
    date: {
      start: 'Start date & time',
      stop: 'End date & time',
    },
  },
}

export type Translations = typeof message

export const en = (message as unknown) as { [name: string]: string }
