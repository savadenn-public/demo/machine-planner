/* istanbul ignore file */
import { createApp } from 'vue'
import App from '@/App.vue'
import AppError from '@/AppError.vue'
import { Router } from '@/router'
import * as Plugins from '@/plugins'
import VCalendar from 'v-calendar'
import '@/registerServiceWorker'
import '@/style/main.scss'
import { initialize } from '@/states'

// Styles
require('typeface-montserrat')

initialize().catch((e) => console.error('Error at initialize', { e }))

try {
  createApp(App).use(Router).use(Plugins.i18n).use(VCalendar).mount('body')
} catch (error) {
  createApp(AppError, { error }).mount('body')
}
