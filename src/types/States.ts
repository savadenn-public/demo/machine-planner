import { Theme, DownloadFormat } from './internal'
import * as Types from '@/types/index'

export interface PwaUpdate {
  refreshing: boolean
  registration: ServiceWorkerRegistration | null
  updateExists: boolean

  read(): void
}

export const SAME_AS_BROWSER = 'settings.sameAsBrowser'

export interface Settings {
  lang: string
  theme: Theme | typeof SAME_AS_BROWSER
  downloadFormat: DownloadFormat
}

/**
 * App state for entry
 */
export interface Entry {
  /**
   * Latest stat in DB at start of app
   */
  latestAtStart: Entry['current']

  /**
   * Current entry
   */
  current: Types.Entry | false

  /**
   * Stop current entry
   */
  stop: Function

  /**
   * Start a new entry, stopping current if needed
   */
  start: Function
}
