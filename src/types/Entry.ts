/**
 * A time Entry
 */
export interface Entry {
  /**
   * Duration of the entry in milliseconds
   */
  duration?: number

  /**
   * Id of the entry
   */
  id: string

  /**
   * Start date
   */
  start: Date

  /**
   * Stop date
   */
  stop?: Date

  /**
   * Content
   */
  content: string

  /**
   * Id of preceding entry
   */
  before?: Entry['id']

  /**
   * id of following entry
   */
  after?: Entry['id']
}
