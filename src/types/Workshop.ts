import { ComputedRef } from 'vue'

export interface Machine {
  id: string
  name: string

  orders: ComputedRef<Order[]>
}

export interface Schedule {
  /**
   * Start date
   */
  start: Date

  /**
   * Stop date
   */
  end: Date
  machineId: Machine['id']
}

export interface Order {
  /**
   * Id of the entry
   */
  id: string

  schedule?: Schedule

  /**
   * Estimated duration in minutes
   */
  duration: number

  user?: string

  title: string

  valid?: boolean

  material: {
    name: string
    color: string
  }
}

export interface Workshop {
  machines: Record<Machine['id'], Machine>
  orders: Record<Machine['id'], Order>
}
