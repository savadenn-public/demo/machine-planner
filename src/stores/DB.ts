import { deleteDB, openDB } from 'idb/with-async-ittr'
import { Schema } from '@/stores/Schema'

const DB_NAME = 'db'

function init() {
  return openDB<Schema>(DB_NAME, 1, {
    upgrade(db) {
      const entrieStore = db.createObjectStore('entries', { keyPath: 'id' })
      entrieStore.createIndex('by-start', 'start', { unique: false })
      entrieStore.createIndex('by-stop', 'stop', { unique: false })
    },
  })
}

let promise = init()

const reset = async function () {
  const DB = await promise
  await DB.close()
  await deleteDB(DB_NAME)
  promise = init()
}

export { promise, reset }
