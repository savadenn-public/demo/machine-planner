import * as Types from '@/types'
import * as DB from './DB'
import { isReactive, toRaw } from 'vue'

/**
 *
 */
export async function storeRead() {
  return (await DB.promise).transaction('entries', 'readonly').store
}

async function storeWrite() {
  return (await DB.promise).transaction('entries', 'readwrite').store
}

/**
 * Gets entry by id
 */
export async function get(id: string) {
  return (await storeRead()).get(id)
}

/**
 * Saves current entry
 *
 * @param entry
 */
export async function put(entry: Types.Entry) {
  return (await storeWrite()).put(isReactive(entry) ? toRaw(entry) : entry)
}

/**
 * Removes entry by id
 * @param id
 */
export async function remove(id: string) {
  return (await storeWrite()).delete(id)
}

/**
 * Retrieves all entries
 */
export async function all() {
  return (await storeRead()).index('by-start').getAll()
}
