import { DBSchema } from 'idb/with-async-ittr'
import * as Types from '@/types'

export interface Schema extends DBSchema {
  entries: {
    value: Types.Entry
    key: string
    indexes: {
      'by-start': Date
      'by-stop': Date
    }
  }
  logs: {
    value: {
      content: string
    }
    key: string
  }
}
