/**
 * Export function for mocking in test
 * @param fileBits
 * @param fileName
 * @param options
 */
export function dataToFile(
  fileBits: BlobPart[],
  fileName: string,
  options?: FilePropertyBag
) {
  return new File(fileBits, fileName, options)
}
