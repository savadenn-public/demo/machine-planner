import { Entry } from '@/types'
import { dump } from 'js-yaml'
import { Formatter } from './internal'

export class YamlFormatter extends Formatter {
  contentType = 'text/yaml;charset=utf-8'
  filename = 'entries.yaml'

  formatSpecific(entries: Entry[]) {
    return dump(entries)
  }
}

/**
 * Singleton instance of YamlFormatter
 */
export const yamlFormatter = new YamlFormatter()
