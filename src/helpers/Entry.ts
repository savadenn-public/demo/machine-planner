import * as Types from '@/types'
import moment from 'moment'
import { v4 as uuid4 } from 'uuid'

export const Entry = {
  /**
   * Stops given entry
   * @param entry
   */
  stop(entry: Types.Entry) {
    if (!entry) return
    entry.stop = moment().startOf('second').toDate()
    entry.duration = moment(entry.stop).diff(entry.start)
  },

  /**
   * Creates a new entry
   */
  new(): Types.Entry {
    return {
      id: uuid4(),
      content: '',
      start: moment().startOf('second').toDate(),
    }
  },

  /**
   * Convert date to displayable one
   * @param date
   */
  humanizeDate(date: Date) {
    let r = moment(date).format('LT')
    if (!moment().isSame(date, 'day')) {
      r = moment(date).format('L') + ' ' + r
    }
    return r
  },

  /**
   * Cast an entry to exportable one
   * @param entry
   */
  export(entry: Partial<Types.Entry>): Partial<Types.Entry> {
    return {
      id: entry.id,
      content: entry.content,
      duration: entry.duration,
      start: entry.start,
      stop: entry.stop,
    }
  },
}
