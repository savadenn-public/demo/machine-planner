import * as Types from '@/types'
import moment from 'moment'
import { v4 as uuid4 } from 'uuid'
import { computed } from 'vue'

export const Machine = {
  /**
   * Creates a new entry
   */
  new(name: string): Types.Machine {
    return {
      id: uuid4(),
      name,
      orders: computed(() => []),
    }
  },

  /**
   * Convert date to displayable one
   * @param date
   */
  humanizeDate(date: Date) {
    let r = moment(date).format('LT')
    if (!moment().isSame(date, 'day')) {
      r = moment(date).format('L') + ' ' + r
    }
    return r
  },
}
