import * as Types from '@/types'
import moment from 'moment'
import { v4 as uuid4 } from 'uuid'

const PARTS = ['Porte', 'Nez', 'Moule', 'Tuyère', 'Turbine', 'Antenne']
const OF = [
  'rafale',
  'Porshe',
  'Tesla',
  'dexter',
  'Latécoère',
  'Ariane',
  'SpaceX',
]
export const GREY_DEFAULT = '#d7d7d7'

const VERSION = [
  ['INVAR', GREY_DEFAULT],
  ['alu 5083', '#ff7f00'],
  ['alu 7075', '#ff7f00'],
  ['alu 7021', GREY_DEFAULT],
  ['aciers 1.2312', '#24a2ec'],
  ['acier 40CMD8+S', GREY_DEFAULT],
  ['acier 55NCDV7+S', GREY_DEFAULT],
  ['acier Z35CD16+S', GREY_DEFAULT],
  ['acier Z35CD17+6S', '#24a2ec'],
  ['acier 2085', '#24a2ec'],
  ['acier forgeA182', '#24a2ec'],
  ['acier F22', '#24a2ec'],
  ['acier F15MN', '#24a2ec'],
  ['tôle', '#9761c1'],
  ['INOX 😉', GREY_DEFAULT],
]

export const Order = {
  /**
   * Creates a new entry
   */
  new(schedule?: Types.Schedule): Types.Order {
    const [materialName, materialColor] = VERSION[
      Math.floor(Math.random() * VERSION.length)
    ]
    return {
      id: uuid4().substr(30),
      material: { name: materialName, color: materialColor },
      title: [
        PARTS[Math.floor(Math.random() * PARTS.length)],
        OF[Math.floor(Math.random() * OF.length)],
        materialName,
      ].join(' '),
      duration: 60 + 15 * Math.floor(Math.random() * 6),
      schedule,
    }
  },

  schedule(order: Types.Order, start: Date, machineId: Types.Machine['id']) {
    let duration = order.duration

    if (order.schedule) {
      duration = moment(order.schedule.end).diff(
        order.schedule.start,
        'minutes',
        true
      )
    }

    order.schedule = {
      start: moment(start).startOf('second').toDate(),
      end: moment(moment(start).add(duration, 'm')).startOf('second').toDate(),
      machineId,
    }
    return order
  },

  /**
   * Convert date to displayable one
   * @param date
   */
  humanizeDate(date: Date) {
    let r = moment(date).format('LT')
    if (!moment().isSame(date, 'day')) {
      r = moment(date).format('L') + ' ' + r
    }
    return r
  },
}
